﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Crud.Models;
using Crud.Db;

namespace Crud.Controllers
{
    public class HomeController : Controller
    {
        private CrudContext context;
        public HomeController(CrudContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var personas = context.Personas;
            return View("Index", personas);
        }
        public IActionResult register()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult register(Persona per) 
        {
            context.Personas.Add(per);
            context.SaveChanges();
            return RedirectToAction("Index");

        }

    }
}
