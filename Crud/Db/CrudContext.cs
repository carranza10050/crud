﻿using Crud.Db.Mapping;
using Crud.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace Crud.Db
{
    public class CrudContext: DbContext

    {
        public DbSet<Persona> Personas{ get; set; }
        

        public CrudContext(DbContextOptions<CrudContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PersonaMap());
           
        }
    }
}
